

<h1>2024.3.29<h1>

HTML结构

css样式

键值对：属性名，属性值，中间以:分开，最后以;结尾

font-size：字体大小

style标签写在head标签里面

<h2>标签选择器<h2>

标签选择器直接用标签名做属性名，多个单词用-连接

css大小写不区分

css 注释CTRL加/

class调用类选择器

class可以调用多个类名，中间用空格分开

css优先级:就近原则

<h2>ID选择器<h2>

id选择器只能调用一个，而且中间不能有空格

规范是一个标签只能有一个id，只能调用一个id选择器。

<h2>通配符选择器<h2>

作用在html标签内的所有元素(通用)(包括html标签，只是没有效果)

<h2>字体<h2>

font-family

多个字体要用英文逗号隔开

sans-serif：非衬线字体（通用字体使用的时候不需要加引号）

windows系统默认微软雅黑 Microseft YaHei

1.中文 2.英文 3.unicode

<h2>字体大小<h2>

font-size: px(像素) Google浏览器最小12px

<h2>字体粗细<h2>

font-weight:(设置数字的时候必须整百的数字)

<h2>文字样式<h2>

font-style

<h2>字体复合属性<h2>

font:font-style font-weight font-size/line-height font-family 字体大小和字体系列是必须的

例：font: italic bold 55px "微软雅黑"

![image-20240330154046271](C:\Users\zhou'xin\AppData\Roaming\Typora\typora-user-images\image-20240330154046271.png)

