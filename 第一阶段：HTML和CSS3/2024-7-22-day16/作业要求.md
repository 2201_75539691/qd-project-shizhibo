作业要求：

注意：选择题做完，相关直接把答案写入当前文档，然后一并和实践代码提交即可。

---

1、请问下列Flex布局中，left与right在页面上的宽度比是多少（B）

d=50+200-200=50

l=left-d\*(2*50/(2\*50+1\*200))=50-50/3=100/3

r=right-d\*(1*200/(2\*50+1\*200))=200-100/3=500/3

l/r=1:5

```html
<style>
    .flex {
        display: flex;
        width: 200px;
        height: 100px;
    }
    .left {
        flex: 3 2 50px;
        background: red;
    }
    .right {
        flex: 2 1 200px;
        background: blue;
    }
</style>
<div class="flex">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

> A. 2:3         B. 1:5         C. 1:3         D. 16:9



2、如果题目1中，`.right`里面的属性改成如下样式，left与right在页面上的宽度比是多少（D）

d=200-50-20=130

l=left+130*(3/2+3)=50+78=128

r=righr+130*(2/2+3)=20+52=72

left:right=16:9

```html
<style>
    .flex {
        display: flex;
        width: 200px;
        height: 100px;
    }
    .left {
        flex: 3 2 50px;
        background: red;
    }
    .right {
        flex: 2 1 20px;
        background: blue;
    }
</style>
<div class="flex">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

> A. 2:3         B. 1:5         C. 1:3         D. 16:9

---

解析：

left + right = 50+200=250，剩余=200px-250px=-50px，相当于超出50，不存在剩余空间，则不需要flex-grow，而采用flex-shrink

left收缩 = 50 - 50 * ( 50*2/(50*2+200*1) ) = 50-50/3 = 100/3
right收缩 = 200 - 50 * ( 200*1/(50*2+200*1) ) = 200-100/3 = 500/3

所以 left:right = 1:5 故选择 B

如果这时候，把 .right { flex: 2 1 200px; } 改成 .right { flex: 2 1 20px; }，那么

left + right = 50+20=70，剩余=200px-70px=130px，还存在剩余空间，则不需要flex-shrink，而采用flex-grow

left扩展 = 130 * (3/3+2) = 78，right扩展 = 130 * (2/3+2) = 52

所以left = 78+50 = 128，right = 52+20 = 72，left:right = 128:72 = 64:36=32:18=16:9

3、根据`05、Flex综合案例.mp4`讲解，独立实现。